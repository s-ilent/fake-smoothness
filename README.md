# Fake Smoothness

# [Can't find the download link? Click here!](https://gitlab.com/s-ilent/fake-smoothness/-/archive/master/fake-smoothness-master.zip)

A simple surface shader for Unity that derives smoothness from the albedo texture and an overlaid pattern. A cheap space-saving alternative to actually making smoothness maps. 

![Demonstration Image](https://gitlab.com/s-ilent/fake-smoothness/-/wikis/uploads/b6a66dc91deb6b40c348a5b9ca7f2f08/2020-09-04_21-27-20.png.jpg)

## Installation

Download the repository. Then place the Shader/ folder with the shader into your Assets/ directory.

## Usage

Place this shader on a material and fiddle with the settings until it looks right. Any random rocky/concrete-y texture will add wonderful depth to the surface. Combine with good reflection probe placement and nobody will notice just how flat it is!

This is designed as a minimal shader, so it doesn't support normal maps. 

## License?

MIT license.
