// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Silent/FakeSmoothness"
{
	Properties
	{
		_MainTex("Albedo", 2D) = "white" {}
		_Metalness("Metalness", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 1
		[Header(Fake Smoothness Settings)]_Crunchiness("Crunchiness", Float) = 0
		[ToggleUI]_OneMinusGrey("Invert Luminance", Float) = 1
		_Pattern("Pattern", 2D) = "white" {}
		_PatternStrength("Pattern Strength", Range( -1 , 1)) = 0
		_PatternScale("Pattern Scale", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _Metalness;
		uniform float _Smoothness;
		uniform sampler2D _Pattern;
		uniform float _PatternScale;
		uniform float _PatternStrength;
		uniform float _Crunchiness;
		uniform float _OneMinusGrey;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode4 = tex2D( _MainTex, uv_MainTex );
			o.Albedo = tex2DNode4.rgb;
			o.Metallic = _Metalness;
			float4 temp_cast_1 = (1.0).xxxx;
			float2 temp_cast_2 = (_PatternScale).xx;
			float2 uv_TexCoord20 = i.uv_texcoord * temp_cast_2;
			float4 lerpResult23 = lerp( temp_cast_1 , tex2D( _Pattern, uv_TexCoord20 ) , _PatternStrength);
			float4 temp_output_7_0 = ( tex2DNode4 - tex2Dlod( _MainTex, float4( uv_MainTex, 0, 7.0) ) );
			float4 lerpResult15 = lerp( temp_output_7_0 , ( temp_output_7_0 * temp_output_7_0 ) , _Crunchiness);
			float4 temp_cast_3 = (0.3333333).xxxx;
			float dotResult8 = dot( lerpResult15 , temp_cast_3 );
			float temp_output_11_0 = saturate( dotResult8 );
			float lerpResult30 = lerp( temp_output_11_0 , ( 1.0 - temp_output_11_0 ) , _OneMinusGrey);
			o.Smoothness = ( _Smoothness * saturate( ( lerpResult23 * lerpResult30 ) ) ).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18100
2228;76;1238;1424;1822.964;868.05;1;True;False
Node;AmplifyShaderEditor.TexturePropertyNode;3;-2105,-286;Float;True;Property;_MainTex;Albedo;0;0;Create;False;0;0;False;0;False;391e309a218cd8e48b6aa5df53ce4623;bbe9d546c22161d45b5d39e2c9da090a;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-1906.686,170.9706;Float;False;Constant;_Float0;Float 0;2;0;Create;True;0;0;False;0;False;7;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;5;-1736.686,94.97058;Inherit;True;Property;_TextureSample1;Texture Sample 1;1;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;MipLevel;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;4;-1830,-434;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;7;-1280.686,-12.02942;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1223.28,-183.9868;Float;False;Property;_Crunchiness;Crunchiness;3;0;Create;True;0;0;False;1;Header(Fake Smoothness Settings);False;0;33.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-1117.28,-65.98676;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;15;-940.2795,4.013245;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-1182.686,221.9706;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;False;0;False;0.3333333;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;8;-937.686,185.9706;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-1025.313,-657.2623;Float;False;Property;_PatternScale;Pattern Scale;7;0;Create;True;0;0;False;0;False;1;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;20;-734.3125,-628.2623;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;11;-689.2795,278.0132;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;24;-329.3125,-715.2623;Float;False;Constant;_Float2;Float 2;5;0;Create;True;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-382.3125,-314.2623;Float;False;Property;_PatternStrength;Pattern Strength;6;0;Create;True;0;0;False;0;False;0;0.237;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-677.4569,354.5508;Inherit;False;Property;_OneMinusGrey;Invert Luminance;4;0;Create;False;0;0;False;1;ToggleUI;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;18;-426.3125,-543.2623;Inherit;True;Property;_Pattern;Pattern;5;0;Create;True;0;0;False;0;False;-1;None;95a6cc99f8d13a440826c94c12b0963e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;10;-559.2795,116.0132;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;30;-403.4569,237.5508;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;23;-3.3125,-400.2623;Inherit;False;3;0;COLOR;1,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;-250.3125,1.737671;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;25;-1.3125,1.737671;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-186.2795,143.0132;Float;False;Property;_Smoothness;Smoothness;2;0;Create;True;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;27;128.5389,-257.0026;Float;False;Property;_Metalness;Metalness;1;0;Create;True;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;201.6875,97.73767;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;474,-133;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Silent/FakeSmoothness;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;1;False;-1;1;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;5;0;3;0
WireConnection;5;2;6;0
WireConnection;4;0;3;0
WireConnection;7;0;4;0
WireConnection;7;1;5;0
WireConnection;16;0;7;0
WireConnection;16;1;7;0
WireConnection;15;0;7;0
WireConnection;15;1;16;0
WireConnection;15;2;17;0
WireConnection;8;0;15;0
WireConnection;8;1;9;0
WireConnection;20;0;21;0
WireConnection;11;0;8;0
WireConnection;18;1;20;0
WireConnection;10;0;11;0
WireConnection;30;0;11;0
WireConnection;30;1;10;0
WireConnection;30;2;28;0
WireConnection;23;0;24;0
WireConnection;23;1;18;0
WireConnection;23;2;19;0
WireConnection;22;0;23;0
WireConnection;22;1;30;0
WireConnection;25;0;22;0
WireConnection;26;0;13;0
WireConnection;26;1;25;0
WireConnection;0;0;4;0
WireConnection;0;3;27;0
WireConnection;0;4;26;0
ASEEND*/
//CHKSM=B842D61E33A75F59E4F24957D48ECD616FAC93C6